@extends('layouts.app')

@section('content')

{{-- Activity:

    Modify this page (welcom.blade.php) to reflect the following specifications:

    1. Have a centered banner at the top showing the Laravel logo

    2 Retrive a randomized list of three blog posts to display under a "Featured Posts" section. 

    Note: 


--}}

    <div class="text-center">
        <img class="w-50" src="https://laravelnews.imgix.net/images/laravel-featured.png">
        <h2>Featured Posts:</h2>
        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                        <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    </div>
                </div>
            @endforeach
        @endif        
    </div>


@endsection