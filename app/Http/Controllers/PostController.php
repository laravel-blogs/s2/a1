<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    //

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        if(Auth::user()){
            //create a new Post object from the post model
            $post = new Post;

            //define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');

            //
            $post->user_id = Auth::user()->id;
    
            //
            $post->save();

            return redirect('/posts');
        }else{
            
            //
            return redirect('/login');
        }
    }

    public function index()
    {
        //get all posts from the database
        $posts = Post::all();

            //1st posts is the same as with the index.blade.php
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome()
    {
        $posts = Post::inRandomOrder()
                ->limit(3)
                ->get();
        return view('welcome')->with('posts', $posts);
    }

    public function myPosts()
    {
        if(Auth::user()){

            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }else{
            return redirect('/login');
        }
    }


    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

}
